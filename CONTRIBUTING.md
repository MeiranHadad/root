# Contributing
1. Nobody can push directly to master.
1. Start a feature branch from master.
1. Consistently, keep your branch up-to-date with master (`git fetch && git rebase origin/master`).  
_- This will save you from conflicts when requesting to merge_
1. Once completed, open a PR.
1. After review and refactor, manager will approve and merge the PR.  
_- This ensures the code will be reviewed and refactored_

# Bugs
### Fixing
Will handle like a feature, see "Contributing".
### Merging
Critical bugs fixes will be merged - in additional to master - also to the latest version branch.  
_- This will allow to embed the fix without waiting for master to become stable_
### Closing
Only the one who opened the issue is responsible for closing it!  
_- This will ensure the bug was resolved correctly_
